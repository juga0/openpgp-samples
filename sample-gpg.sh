#!/bin/bash
export GNUPGHOME="$(cd $(dirname $0) && pwd)/.gnupg"
exec gpg "$@"
